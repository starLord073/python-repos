import requests
import json
import pickle

url = 'https://connect.sandbox.achievers.com/middleware/api/shared/getlogs'

for i in range(22,23):
    notificationObjects = []
    # f = open('logs.txt', 'a')
    count = 0 + (10 * i)
    end = count + 8
    while (count < end ):
        print('Total logs saving :',count+i)
        params = {
            'appType':'middleware',
            'count':1000,
            'page':count+1,
        }
        response = requests.get(url, params).text

        # f.write(response.text)
        count += 1
        responseJson = json.loads(response)
        allRows = responseJson["rows"]
        for row in allRows:
            log = row["description"]
            logJSON = json.loads(log)
            notificationPayload = logJSON["notificationPayload"]
            notificationPayload['id'] = row["id"]
            notificationObjects.append(notificationPayload)
    with open('.//serialized_logs//logData_'+str(1+(i * 10000))+'-'+str(10000 + (i*10000))+'.pkl', 'wb') as f:
        pickle.dump(notificationObjects,f)






# print(response)
# threads = []
# for i in range(1):
#     t = threading.Thread(target=do_request)
#     t.daemon = True
#     threads.append(t)

# for i in range(1):
#     threads[i].start()

# for i in range(1):
#     threads[i].join()